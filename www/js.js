	
	function update(element) {
		
		var idInfo = element.getAttribute("idInfo")
		var percentage = element.value;

		var xhr;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		    xhr = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // IE 8 and older
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var data = "idInfo=" + idInfo +"&percentage=" + percentage;
			 xhr.open("POST", "ajax.php", true); 
		     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
		     xhr.send(data);

		     xhr.onreadystatechange = display_data;
			function display_data() {
			 if (xhr.readyState == 4) {
		      if (xhr.status == 200) {
		      var arr = new Array();
		       var arr = JSON.parse(xhr.responseText);
		       //alert(xhr.responseText);	 

		   
 			 highestScore(arr['highestScore']);
 			 lowestScore(arr['lowestScore']);
 			 document.getElementById("classAverageScore").innerHTML = arr['classAverageScore'];
 			 document.getElementById("classAverageScoreCorner").innerHTML = arr['classAverageScore'];
 			 document.getElementById("numberBelowAverageScore").innerHTML = arr['numberBelowAverageScore'];
 			 //console.log(arr['students'])
 			
 			 	
 			  studentsAverage(arr['students']);
 			  testAverage(arr['tests'])
			  

		      } else {
		        alert('There was a problem with the request.');
		      }
		     }
			}
	
	}

	


	function highestScore(str) {
	document.getElementById("highestScore").innerHTML = "<tr><td colspan='4'><b>Student(s) with highest score.</b></td></tr>"+str; 
	}

	function lowestScore(str) {
	document.getElementById("lowestScore").innerHTML = "<tr><td colspan='4'><b>Student(s) with lowest score.</b></td></tr>"+str; 
	}

	function studentsAverage(obj) {
		

		Object.keys(obj).forEach(function (key) {
   				console.log("stud" + obj[key]['id'])
   				document.getElementById("stud" + obj[key]['id']).innerHTML  = obj[key]['average'];
		
		});
	}
	function testAverage(obj) {
		

		Object.keys(obj).forEach(function (key) {
   				console.log("test" + key)
   				document.getElementById("test" + obj[key]['id']).innerHTML  = obj[key]['average'];
		
		});		
	}