<?php 

require_once('db.php');
/**
* 
*/
class Student
{
	private $db;
	protected $studentsAverageScore;
	protected $classAverage;
	public $infoStudents;
	
	function __construct($db)
	{
		$this->db = $db;
        $this->classAverageScore();
	}

	public function update($idInfo, $percentage)
	{
		$sql = "UPDATE info SET percentage=".$percentage." WHERE id=".$idInfo;

		if($this->db->query($sql) === TRUE) {
             $this->classAverageScore();
    		return 1;
		} else {
		    return 0;
		}

	}

	public function highestScore()
	{
		
		$data = $this->studentsAverageScore;
		$max = max( array_column( $data, 'percentage' ) );
		$key =array_keys(array_column( $data, 'percentage','id' ), $max);

		foreach ($key as  $value) {
			$highestScore[] = $this->studentsAverageScore[$value];
		}
		 //print_arr($highestScore);
		return $highestScore;
		
	}
	public function lowestScore()
	{
		
		$data = $this->studentsAverageScore;
		$min = min( array_column( $data, 'percentage' ) );
		$key =array_keys(array_column( $data, 'percentage','id' ), $min);
		
		foreach ($key as  $value) {
			$lowestScore[] = $this->studentsAverageScore[$value];
		}
		 //print_arr($lowestScore);
		return $lowestScore;

		
	}
	
	public function studentAverageAllTests($id)
	{
   // $sql ="Select s.name,s.surname, s.id, t.name test, i.percentage  from info i
  //   	Join students s on s.id=i.idStudent
  //   	Join test t on t.id=i.idTest
  //   	Where s.id=".$id;
	 
     	$sql ="Select s.id, i.percentage  from info i
     	Join students s on s.id=i.idStudent
     	Join test t on t.id=i.idTest
     	Where s.id=".$id;
     	
     	$result = $this->db->query($sql);
        while ($obj = $result->fetch_object()) {
        	$arr[] = $obj->percentage;
    	}
    	
    	$average_of_percentage = array_sum($arr) / count($arr); 
    	 //print_arr($arr);
    	 //echo "average = ".$average_of_percentage;
    	return $average_of_percentage;

	}


	
	public function averageMarkEachTest($id)
	{

     	$sql ="Select t.id,s.name, t.name test, i.percentage  from info i
     	Join students s on s.id=i.idStudent
     	Join test t on t.id=i.idTest
     	Where t.id=".$id;
     	
     	$result = $this->db->query($sql);
        while ($obj = $result->fetch_object()) {
        	
        	$arr[] = $obj->percentage;
    	}
    	
    	$average_of_percentage = array_sum($arr) / count($arr); 
    	 //print_arr($arr);
    	 //echo "average = ".$average_of_percentage;
    	return $average_of_percentage;
     	
	}

	public function classAverageScore($action = 0)
	{
		$sql = "Select id, name, surname from students";
		$result = $this->db->query($sql);
        while ($obj = $result->fetch_object()) {
        	 //print_arr($obj);
        	$arr[$obj->id] = $this->studentAverageAllTests($obj->id);
        	$students[$obj->id]['id'] = $obj->id;
        	$students[$obj->id]['name'] = $obj->name;
        	$students[$obj->id]['surname'] = $obj->surname;
        	$students[$obj->id]['percentage'] = $arr[$obj->id];
        
    	}
    	$this->studentsAverageScore = $students;
		//print_arr($this->studentsAverageScore);    	
    	$this->classAverage = array_sum($arr) / count($arr); 

    	if($action) 
    		return $this->classAverage;

    	
	}

	public function numberBelowAverageScore()
	{
		$data = $this->studentsAverageScore;
		$i=0;
		foreach( array_column( $data, 'percentage' ) as $k => $v){
		if($v< $this->classAverage ) $i++;
		}
		return $i;

	}


    public function getStudents($action = 0 )
    {



    		$sql = "Select id, name, surname from students";
    		$result = $this->db->query($sql);
            while ($obj = $result->fetch_object()) {

            	 //print_arr($obj);
            	$arr[$obj->id] = $this->studentAverageAllTests($obj->id);
            	$students[$obj->id]['id'] = $obj->id;
            	$students[$obj->id]['name'] = $obj->name;
            	$students[$obj->id]['surname'] = $obj->surname;
            	$students[$obj->id]['average'] = $arr[$obj->id];
            	
            	$sql2 ="Select i.id idInfo, s.id, t.name test, i.percentage  from info i
        		Join students s on s.id=i.idStudent
        		Join test t on t.id=i.idTest
        		Where s.id=".$obj->id;
         	
         		$result2 = $this->db->query($sql2);
            	
            	while ($obj2 = $result2->fetch_object()) {
            		$students[$obj->id]['test'][$obj2->idInfo]['idInfo'] = $obj2->idInfo;  
            		$students[$obj->id]['test'][$obj2->idInfo]['test'] = $obj2->test;  
            		$students[$obj->id]['test'][$obj2->idInfo]['percentage'] = $obj2->percentage;  
        		}
        	


            
        	}

        	$this->infoStudents = $students;
      
        	
        	if($action) return $this->infoStudents;
    	 
    }

    public function getTest()
    {
        
        $sql ="Select id, name  from test";
        
        $result = $this->db->query($sql);
        while ($obj = $result->fetch_object()) {
            
            $arr[$obj->id]['id'] = $obj->id;
            $arr[$obj->id]['name'] = $obj->name;
            $arr[$obj->id]['average'] = $this->averageMarkEachTest($obj->id);
        }
        return $arr;
    }


}//End Student


?>