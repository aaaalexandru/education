<?php 

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once dirname(__FILE__).'/inc/db.php';
include_once dirname(__FILE__).'/inc/student.class.php';

$student = new Student($db);

$students = $student->getStudents(1);


$tests = $student->getTest();
$count = count($tests);
// echo "count = ". $count;
// print_arr($tests);

$highestScore =  $student->highestScore();
$lowestScore =	$student->lowestScore();
?>


<!DOCTYPE html>
<html>
<head>

	<title> Students </title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="center">
	<h1>Students</h1>
	<p>Student lists.</p>

	<table border="1">
		<tr>
			<td colspan="3"></td>
			<td colspan="<?php echo $count; ?>">Tests</td>
			<td>Average</td>
		</tr>
		<tr>
			<td>Nr</td>
			<td>Name</td>
			<td>Surname</td>
			<?php 
				foreach ($tests as $value) {
					echo "<td>".$value['name']."</td>";
				}
			?>
			<td></td>
		</tr>

		<?php
		  $i =1;
		  foreach ($students as  $value) {
			
		 ?>
		<tr>
			<td><?php echo $i;?></td>
			<td><?php echo $value['name'];?></td>
			<td><?php echo $value['surname'];?></td>
			<?php 
				foreach ($value['test'] as  $v) {
					echo "<td><input value='".$v['percentage']."' onchange='update(this)' idInfo='".$v['idInfo']."'></td>";
				}
			 ?>
			 <td id="stud<?php echo $value['id'] ?>"><?php echo $value['average'] ?></td>
		</tr>

	<?php $i++;} ?>
		<tr>
			<td colspan="3">Average tests</td>
			<?php 
				foreach ($tests as $value) {
					echo "<td id='test".$value['id']."'>".$value['average']."</td>";
				}
			?>
			<td id="classAverageScoreCorner"><?php echo $student->classAverageScore(1); ?></td>
		</tr>

	</table>


	<h1>Statistics</h1>

	<table  id="highestScore">
		<tr>
			<td colspan="4"><b>Student(s) with highest score.</b></td>
		</tr>
		
		<?php $i=1;
			 foreach ($highestScore as  $value) {
			
			 ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $value['name'];?></td>
				<td><?php echo $value['surname'];?></td>
				<td><?php echo $value['percentage'];?></td>
			</tr>	
			<?php $i++; } ?>	
			</table>

		<table id="lowestScore">
		<tr>
			<td colspan="4"><b>Student(s) with lowest score.</b></td>
		</tr>
		
	
		<?php $i=1;
			 foreach ($lowestScore as  $value) {
			
			 ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $value['name'];?></td>
				<td><?php echo $value['surname'];?></td>
				<td><?php echo $value['percentage'];?></td>
			</tr>	
			<?php $i++; } ?>
		</table>
		<table>	
			<tr>
				<td colspan="3"><b>Class average score:</b></td>
				<td id="classAverageScore"><?php echo $student->classAverageScore(1); ?></td>
			</tr>
			<tr>
				<td colspan="3"><b>Number of students below the average score:</b></td>
				<td id="numberBelowAverageScore"><?php echo $student->numberBelowAverageScore(); ?></td>
			</tr>
			
		</table>
</div>


	<script type="text/javascript" src="js.js">
	

</script>
</body>
</html>