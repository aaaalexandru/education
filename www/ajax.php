<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once dirname(__FILE__).'/inc/db.php';
include_once dirname(__FILE__).'/inc/student.class.php';

$idInfo = $_POST['idInfo'];
$percentage = $_POST['percentage'];
if(!is_numeric(($percentage)) || $percentage > 100) return;




$student = new Student($db);

if($student->update($idInfo,$percentage))
{
	$students = $student->getStudents(1);
	$tests = $student->getTest();
	$count = count($tests);
	// echo "count = ". $count;
	 //print_arr($students);

	$highestScore =  $student->highestScore();
	$lowestScore =	$student->lowestScore();
	$classAverageScore = $student->classAverageScore(1);
	$numberBelowAverageScore = $student->numberBelowAverageScore();

	$arr['students'] = $students;
	$arr['tests'] = $tests;
	 $i=1;
	 	$highestScoreStr = "";
		foreach ($highestScore as  $value) {
			
			 
		$highestScoreStr .= 
			"<tr>
				<td>".$i."</td>
				<td>".$value['name']."</td>
				<td>".$value['surname']."</td>
				<td>".$value['percentage']."</td>
			</tr>";
		$i++; 
		} 	
	//$highestScore = 
	$arr['highestScore'] = $highestScoreStr;
		 $i=1;
		 $lowestScoreStr ="";
		foreach ($lowestScore as  $value) {
			
			 
		$lowestScoreStr .= 
			"<tr>
				<td>".$i."</td>
				<td>".$value['name']."</td>
				<td>".$value['surname']."</td>
				<td>".$value['percentage']."</td>
			</tr>";
		$i++; 
		} 
	$arr['lowestScore'] = $lowestScoreStr; 
	$arr['classAverageScore'] = $classAverageScore; 
	$arr['numberBelowAverageScore'] = $numberBelowAverageScore; 

	echo json_encode($arr) ;	
}



?>